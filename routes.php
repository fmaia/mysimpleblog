<?php 

function call($controller, $action){
	require_once("controllers/".$controller.
					"_controller.php");
	switch ($controller) {
		case "posts":
			require_once("models/post.php");
			$controller = new PostsController();
			break;
	}
	$controller->{$action}();
}

call($controller, $action);

?>