<?php
  class Post {
    public $id;
    public $autor;
    public $conteudo;

    public function __construct($id, $autor, $conteudo) {
      $this->id      = $id;
      $this->autor  = $autor;
      $this->conteudo = $conteudo;
    }

    public static function all() {
      $list = [];
      $db = Db::getInstance();
      $req = $db->query('SELECT * FROM posts');
      foreach($req->fetchAll() as $post) {
        $list[] = new Post($post['id'], $post['autor'], $post['conteudo']);
      }

      return $list;
    }

    public static function insert($post){
      $db = Db::getInstance();
      $req = $db->prepare("INSERT INTO 
                      posts(autor, conteudo)
                      VALUES (:autor, :conteudo)");
      $req->execute(array("autor"=>$post->autor,
                          "conteudo"=>$post->conteudo));
    }

    public static function delete($id){
      $db = Db::getInstance();
      $id = intval($id);
      $req = $db->prepare("DELETE FROM posts
                            WHERE id=:id");
      $req->execute(array("id"=>$id));
    }

    public static function update($post){
      $db = Db::getInstance();
      $req = $db->prepare("UPDATE posts
                      SET autor=:autor, conteudo=:conteudo
                      WHERE id=:id");
      $req->execute(array("autor"=>$post->autor,
                          "conteudo"=>$post->conteudo,
                          "id"=>$post->id));
    }

    public static function find($id) {
      $db = Db::getInstance();
      $id = intval($id);
      $req = $db->prepare('SELECT * FROM posts WHERE id = :id'); 
      $req->execute(array('id' => $id));
      $post = $req->fetch();
      return new Post($post['id'], $post['autor'], $post['conteudo']);
    }
  }
?>