<?php

class PostsController {
	public function show(){
		$posts = Post::all();
		require_once("views/posts/insert.php");
		require_once("views/posts/show.php");
	}	

	public function insert(){
		Post::insert(new Post(0, $_POST["autor"],
								$_POST["conteudo"]
					));
		$posts = Post::all();
		require_once("views/posts/insert.php");
		require_once("views/posts/show.php");
	}

	public function delete(){
		Post::delete($_GET["id"]);
		$posts = Post::all();
		require_once("views/posts/insert.php");
		require_once("views/posts/show.php");
	}

	public function update(){
		if(isset($_POST["id"])){
			Post::update(
			new Post($_POST["id"], $_POST["autor"],
						$_POST["conteudo"]));
			$posts = Post::all();
			require_once("views/posts/insert.php");
			require_once("views/posts/show.php");
		} else {
			$post=Post::find($_GET["id"]);
			require_once("views/posts/update.php");
		}
	}
}

?>